Automatically fills fields with default values when creating a Jira Issue.

Step 1: Install https://www.tampermonkey.net

Step 2: Add jira-defaults.js (utilities > install from url > enter url to raw js file)

Step 3: Configure your jira url (edit > settings > user includes)

Step 4: Add your own default values (edit > getPrefilledFields function)

:)