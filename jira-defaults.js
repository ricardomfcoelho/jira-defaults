// ==UserScript==
// @name         Jira  Defaults
// @namespace    edirect
// @version      1
// @description  Fill default values when creating Jira Issue
// @author       Coelho
// @require      https://code.jquery.com/jquery-3.4.1.min.js
// @match        https://edirectinsure.atlassian.net/*
// @grant        GM_addValueChangeListener
// @grant        GM_setValue
// @grant        GM_addStyle
// ==/UserScript==

(function () {
    'use strict';
    const $myJQuery = jQuery.noConflict(true);

    function getPrefilledFields(dialogContent) {
        return [
            new ElementConfig("#customfield_11800-4", true, "Delta"),
            new ElementConfig("#customfield_12003-11", true, "bolttech-platform"),
        ]
    }


    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(event,content,q) {
        const parentId = AJS.$(content).parent().attr('id');
        if ('create-issue-dialog' === parentId || 'create-subtask-dialog' === parentId || 'software-create-issue-dialog' === parentId) {
            const dialog = $myJQuery(".jira-dialog");
            const dialogContent = (dialog).find(".jira-dialog-content");
            doIt(dialogContent);
        }
    });

    function doIt(dialogContent) {
        const prefilledFields = getPrefilledFields(dialogContent);

        prefilledFields.forEach(function(prefilledField) {
            prefilledField.check(dialogContent);
            prefilledField.apply(dialogContent);
        })
    }

    function ElementConfig(selector, value, readableName) {
        this.selector = selector;
        this.value = value;
        this.readableName = readableName;
        this.apply = function (parentElement) {
            if (this.value == null) {
                return;
            }
            const element = parentElement.find(this.selector);
            if (element.is(":checkbox, :radio")) {
                element.prop('checked', this.value);
            } else if (element.is("input, textarea")) {
                element.val(this.value);
            } else if (element.is("select")) {
                element.val(this.value);
                //scroll to selected
                setTimeout(function () {
                    const optionTop = element.find("option:selected").offset().top;
                    const selectTop = element.offset().top;
                    element.scrollTop(element.scrollTop() + (optionTop - selectTop));
                }, 100);
            }
        };
        this.check = function (parentElement) {
            const element = parentElement.find(this.selector);
            if (element.parents(".qf-form.qf-configurable-form").length === 1 && element.parents(".qf-field-active").length === 0) {
                alert("Missing required field '" + this.readableName + "'");
                return false;
            }
            if (element.is(":checkbox, :radio")) {
                return parentElement.find("[name='"+element.attr("name")+"']:checked").length != 0;
            } else if (element.is("input, textarea")) {
                return element.val().length != 0;
            } else if (element.is("select")) {
                return element.val().length != 0;
            } else {
                return true;
            }
        }
    }
})();
